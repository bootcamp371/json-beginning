window.onload = function () {

    listUsers();
}

function listUsers() {
   
    const tBody = document.getElementById("usersTblBody");
    let myURL = "https://jsonplaceholder.typicode.com/users/";
    fetch(myURL)
        .then((response) => response.json())
        .then((data) => {
            

            for (let i = 0; i < 6; i++) {
                
                buildTable(tBody, data[i]);

            }
        });

}

function buildTable(tbody, theTicket) {


    // Create an empty <tr> element and add it to the last
    // position of the table
    let row = tbody.insertRow(-1);
    // Create new cells (<td> elements) and add text
    let cell1 = row.insertCell(0);
    cell1.innerHTML = theTicket.name;
    let cell2 = row.insertCell(1);
    cell2.innerHTML = theTicket.username;
    let cell3 = row.insertCell(2);
    cell3.innerHTML = theTicket.email;
    let cell4 = row.insertCell(3);
    cell4.innerHTML = theTicket.address.street;
    let cell5 = row.insertCell(4);
    cell5.innerHTML = theTicket.address.city;
    let cell6 = row.insertCell(5);
    cell6.innerHTML = theTicket.phone;

}


