window.onload = function () {

    const myButton = document.getElementById("myButton");
    myButton.onclick = onMyButtonClicked;
}

function onMyButtonClicked() {

    const myNum = document.getElementById("idNum");
    const myDiv = document.getElementById("myDiv");
    let myrealnum = myNum.value;
    let myURL = "https://jsonplaceholder.typicode.com/todos/" + myrealnum;
    fetch(myURL)
        .then((response) => response.json())
        .then((data) => {
            
            let message = data.title;
            myDiv.innerHTML = message;
        });


    return false;
}