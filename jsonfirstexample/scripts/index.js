let data = {
    "status": "success",
    "data": [
        {
            "id": 34,
            "first_name": "Dev",
            "last_name": "Expert",
            "email": "remmyghty@gmail.com",
            "approved": 1,
            "verified": 1,
            "image": "",
            "required_fields": 1,
            "team_member": 1,
            "visible": 1,
            "position": "",
            "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,",
            "created_at": "2017-12-07 14:47:09",
            "updated_at": "2021-01-05 13:04:06",
            "provider": "",
            "identifier": "",
            "settings": null,
            "remember_token": null,
            "otp_verification": 1,
            "otp_disabled_at": null,
            "imageURL": null,
            "header_logo_url": null,
            "font_family_setting": null,
            "font_size_setting": null,
            "header_logo_position": null,
            "header_company_position": null,
            "header_company_text": null,
            "header_font_size": null,
            "footer_text": null,
            "footer_position": null,
            "footer_font_size": null
        },
        {
            "id": 84,
            "first_name": "React",
            "last_name": "Expert",
            "email": "apurav2007@outlook.com",
            "approved": 1,
            "verified": 1,
            "image": "gf14JFvN.png",
            "required_fields": 1,
            "team_member": 1,
            "visible": 1,
            "position": "Programmer ",
            "description": "Expert in React.js and Laravel\n\n",
            "created_at": "2019-07-03 11:10:37",
            "updated_at": "2020-07-30 14:10:05",
            "provider": "",
            "identifier": "",
            "settings": "{\"document_settings\":{\"fontfamily\":\"verdana\",\"fontsize\":\"10\",\"headerlogoposition\":\"left\",\"addresstxt\":\"header from admin\",\"headerfontsize\":\"12\",\"headercompanyposition\":\"center\",\"footertxt\":\"Footer from admin\",\"footerfontsize\":\"12\",\"footerposition\":\"left\",\"headerlogo\":\"Wkarv6YH.png\"},\"share_email_settings\":{\"subject\":\"Ik deel graag dit document met jou set from admin end for live testing 1234\",\"message\":\"<p>Hoi,&nbsp;<\\\/p>\\n<p>checking sharing email set from admin end<\\\/p>\\n<p>&nbsp;<\\\/p>\\n<p>for live testing<\\\/p>\\n<p>&nbsp;<\\\/p>\\n<p>1234<\\\/p>\"},\"share_draft_instruction\":{\"message\":\"Hello, This is default message set from admin end for live testing 1234\"},\"default_settings\":{\"share_email_subject\":\"Ik deel graag dit document met jou\",\"share_email_message\":\"<p>Hoi,&nbsp;<\\\/p>\\n<p>Bekijk het met jou gedeelde bestand:&nbsp;[[LINK]]<\\\/p>\",\"share_draft_instruction_message\":\"Hello, This is default message set from Admin for Testing Purpose\"}}",
            "remember_token": null,
            "otp_verification": 1,
            "otp_disabled_at": null,
            "imageURL": "https:\/\/api.documentmaken.nl\/images\/users\/gf14JFvN.png",
            "header_logo_url": "https:\/\/api.documentmaken.nl\/images\/documentlogos\/84\/Wkarv6YH.png",
            "font_family_setting": "verdana",
            "font_size_setting": "10",
            "header_logo_position": "left",
            "header_company_position": "center",
            "header_company_text": "header from admin",
            "header_font_size": "12",
            "footer_text": "Footer from admin",
            "footer_position": "left",
            "footer_font_size": "12"
        },
        {
            "id": 104,
            "first_name": "UX & SEO",
            "last_name": "Expert",
            "email": "info@mmadvocatuur.nl",
            "approved": 1,
            "verified": 1,
            "image": "",
            "required_fields": 1,
            "team_member": 1,
            "visible": 1,
            "position": "",
            "description": "",
            "created_at": "2019-09-11 14:35:02",
            "updated_at": "2021-01-05 13:05:17",
            "provider": "",
            "identifier": "",
            "settings": null,
            "remember_token": null,
            "otp_verification": 1,
            "otp_disabled_at": null,
            "imageURL": null,
            "header_logo_url": null,
            "font_family_setting": null,
            "font_size_setting": null,
            "header_logo_position": null,
            "header_company_position": null,
            "header_company_text": null,
            "header_font_size": null,
            "footer_text": null,
            "footer_position": null,
            "footer_font_size": null
        },
        {
            "id": 182,
            "first_name": "Nadesh",
            "last_name": "Ark info",
            "email": "nadesh@arkinfotec.com",
            "approved": 1,
            "verified": 0,
            "image": "",
            "required_fields": 1,
            "team_member": 1,
            "visible": 1,
            "position": "",
            "description": "",
            "created_at": "2021-01-11 13:53:34",
            "updated_at": "2021-02-23 04:48:28",
            "provider": "",
            "identifier": "",
            "settings": null,
            "remember_token": null,
            "otp_verification": 1,
            "otp_disabled_at": null,
            "imageURL": null,
            "header_logo_url": null,
            "font_family_setting": null,
            "font_size_setting": null,
            "header_logo_position": null,
            "header_company_position": null,
            "header_company_text": null,
            "header_font_size": null,
            "footer_text": null,
            "footer_position": null,
            "footer_font_size": null
        }
    ]
};

//   1. Parsing
// Parse the example.json to a JavaScript object. Access the second element from the data array.
// Print all the key value pairs.

// 2. Stringify
// Create an object Person, give person a name, age and address
// Turn this object into json
// Write the result to the console

// let str = JSON.stringify(jobs, ["test", "title"]);

// console.log(str);

let str = JSON.stringify(data);
//console.log(str);

let parse = JSON.parse(str);
//console.log(parse);

console.log("_____________________________________");
console.log(parse.data[1]);
console.log("_____________________________________");
console.log(parse.data[1].first_name);

let person = {
    firstname: "mark",
    lastname: "Galasso",
    job: "Space Cowboy"

}

let stringyfy = JSON.stringify(person);
console.log(stringyfy);

