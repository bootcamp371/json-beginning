let cities = [
    {
        name: "Detroit, MI",
        latitude: 42.3834,
        longitude: -83.1024
    },
    {
        name: "New York, NY",
        latitude: 40.6943,
        longitude: -73.9249
    },
    {
        name: "Los Angeles, CA",
        latitude: 34.1139,
        longitude: -118.4068
    },
    {
        name: "Las Vegas, NV",
        latitude: 36.2333,
        longitude: -115.2654
    },
    {
        name: "Orlando, FL",
        latitude: 28.4772,
        longitude: -81.3369
    },
    {
        name: "Dallas TX",
        latitude: 32.7936,
        longitude: -96.7662
    },
    {
        name: "Seattle, WA",
        latitude: 47.6062,
        longitude: -122.3321
    },
    {
        name: "Fairbanks, Ak",
        latitude: 64.8434809830424,
        longitude: -147.68823330582902
    }
];


window.onload = function () {

    loadCities();
    document.getElementById("cityList").onchange = onSelectClick;
}

function sortCities() {
    cities.sort(function (a, b) {
        if (a.name < b.name) {
            return -1;
        }
        else if (a.name == b.name) {
            return 0;
        }
        else { return 1; }
    });

}
function loadCities() {

    const cityList = document.getElementById("cityList");

    sortCities();

    cityList.appendChild(new Option("Select an city", "Select an city"));
    for (let i = 0; i < cities.length; i++) {
        cityList.appendChild(new Option(cities[i].name, cities[i].name));
    }
    return false;

}

function onSelectClick() {

    const myCity = document.getElementById("cityList");
    const table = document.getElementById('wholeTable');

    let tableHeaderRowCount = 1;
    let rowCount = table.rows.length;

    for (var i = tableHeaderRowCount; i < rowCount; i++) {
        table.deleteRow(tableHeaderRowCount);
    }

    if (myCity.selectedIndex == 0) {
        return false;
    }
    let myURL = "https://api.weather.gov/points/" +
        cities[myCity.selectedIndex - 1].latitude + "," +
        cities[myCity.selectedIndex - 1].longitude;

    fetch(myURL)
        .then((response) => response.json())
        .then((data) => {

            let secondURL = data.properties.forecast;
            secondURLFetch(secondURL);

        });

}

function secondURLFetch(secondURL) {
    const tBody = document.getElementById("tableBody");
    fetch(secondURL)
        .then((response) => response.json())
        .then((data2) => {

            for (let i = 0; i < data2.properties.periods.length; i++) {

                buildTable(tBody, data2, i);

            }
        });
}

function buildTable(tbody, inputRecord, i) {

    // Create an empty <tr> element and add it to the last
    // position of the table
    let row = tbody.insertRow(-1);
    // Create new cells (<td> elements) and add text
    let cell1 = row.insertCell(0);
    cell1.innerHTML = inputRecord.properties.periods[i].name;
    
    let cell2 = row.insertCell(1);
    cell2.innerHTML = inputRecord.properties.periods[i].temperature + " " +
        inputRecord.properties.periods[i].temperatureUnit;
        let cell3 = row.insertCell(2);
    let newImage = document.createElement("img");
    newImage.src = inputRecord.properties.periods[i].icon;
    cell3.appendChild(newImage);
    let cell4 = row.insertCell(3);
    cell4.innerHTML = inputRecord.properties.periods[i].shortForecast;
    let cell5 = row.insertCell(4);
    cell5.innerHTML = inputRecord.properties.periods[i].windSpeed + " " +
        inputRecord.properties.periods[i].windDirection;
   


    // let cell1 = row.insertCell(0);
    // cell1.innerHTML = inputRecord.properties.periods[i].name;
    // let cell2 = row.insertCell(1);
    // cell2.innerHTML = inputRecord.properties.periods[i].temperature + " " +
    //     inputRecord.properties.periods[i].temperatureUnit;
    // let cell3 = row.insertCell(2);
    // cell3.innerHTML = inputRecord.properties.periods[i].shortForecast;
    // let cell4 = row.insertCell(3);
    // cell4.innerHTML = inputRecord.properties.periods[i].windSpeed + " " +
    //     inputRecord.properties.periods[i].windDirection;
    // let cell5 = row.insertCell(4);
    // let newImage = document.createElement("img");
    // newImage.src = inputRecord.properties.periods[i].icon;
    // cell5.appendChild(newImage);

    

}